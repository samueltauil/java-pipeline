## Notes

assuming nexus is provisioned https://github.com/samueltauil/nexus
assuming jenkins is provisioned or configured to auto-provision

create 3 project by web ui, dev, qa and ci

1 - add the build and deploy env vars of maven:
MAVEN_MIRROR_URL http://nexus.ci.svc.cluster.local:8081/repository/maven-public/
2 - create the pipeline on ci project
3 - execute the pipeline 
